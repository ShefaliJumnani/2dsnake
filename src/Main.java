import java.awt.Color;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      
		//make a JFrame-->window in which game runs
		JFrame obj=new JFrame();
		
		//create the object of 2nd class:
		Gameplay gameplay=new Gameplay();
		//set the properties of frame:
		
		obj.setBounds(10, 10, 905, 700);
		obj.setBackground(Color.DARK_GRAY);
		obj.setResizable(false);
		obj.setVisible(true);
		obj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		obj.add(gameplay); //add the object of gameplay to the object of JFrame
	}

}
