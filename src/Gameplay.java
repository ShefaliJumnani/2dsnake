import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.Timer;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

//implement a panel and add it inside frame



public class Gameplay extends JPanel implements KeyListener,ActionListener{
private ImageIcon titleImage; //create the object of ImageIcon class
	
	//for the positions of snake inside the panel,we need to define two arrays for x position and y position
	private int[] snakexlength=new int[750];
	private int[] snakeylength=new int[750];
	
	
	//variables(4)to detect on which side snake is moving:
	private boolean left=false;
	private boolean right=false;
	private boolean up=false;
	private boolean down=false;
	
	//variables(4) to detect direction of snake's face:
	private ImageIcon rightmouth;
	private ImageIcon upmouth;
	private ImageIcon downmouth;
	private ImageIcon leftmouth;
	
	//default length of snake:
	private int lengthofsnake = 3;
	
	//for the movement of snake,we need to add a timer class that will manage the speed of snake inside the panel.
	private Timer timer;
	//set the speed of timer:
	private int delay=100;
	
	//define the image of snakebody(snakeImage):
	private ImageIcon snakeimage;
	
	
	//set the position of enemy variables:
	//default positions for the pickup:
	private int[] enemyxpos= {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	private int[] enemyypos= {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	private ImageIcon enemyimage;
	
	private Random random=new Random();
	
	private int xpos=random.nextInt(34); 
	private int ypos=random.nextInt(23); //23 is the no of elements in ypos.
	
	//variables to add the scores and length of snake at the top-right corner:
	private int score=0;
	
	private int moves=0;
	
	//constructor:
	//add the default position of snake inside the panel:
	public Gameplay() {
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		
		//instantiate timer class:
		timer=new Timer(delay, this); //the args passed sets the speed of snake and the context of the snake
		timer.start();
	}
	
	public void paint(Graphics g) {     
		
		if(moves==0) {
			
			//if the game has just started,set the default position of snake to this.If the game has already started and we are in a play mode then dont check this condition as moves will be incremented and will not be equalt to 0.
			//any code that will write in this block will be executed at just the start of the game
			
			snakexlength[2]=50;
			snakexlength[1]=75;
			snakexlength[0]=100; //defining 3 var since initial snake size 3
			
			snakeylength[2]=100;
			snakeylength[1]=100;
			snakeylength[0]=100;
			
		}
		//draw title image border:
		g.setColor(Color.white);
		g.drawRect(24, 10, 851, 55);
		
		//draw title image inside that border:
		titleImage=new ImageIcon("snaketitle.jpg");
		titleImage.paintIcon(this, g, 25, 11);      //set the context to this,object should be the name of the graphics object g,set the x and y axis.
		
		//draw the border of playing area of snake(gameplay):
		g.setColor(Color.white);
		g.drawRect(24, 74, 851, 577);
		
		//implement the background colour of the playing area of snake(draw background for gameplay):
		g.setColor(Color.black);
		g.fillRect(25, 75, 850, 575);  //pre-calculated values necessary to run the games efficiently (all the enemy stripes,enemy circles and snake images are set to 25*25.So,the width and height of gameplay area should be a multiple of 25.)
		
		
		//draw scores:
		g.setColor(Color.white);
		g.setFont(new Font("monospaced",Font.PLAIN,14));
		//draw the font:
		g.drawString("Score: "+score,780,30);
		
		//draw length of snake:
		g.setColor(Color.white);
		g.setFont(new Font("monospaced",Font.PLAIN,14));
		g.drawString("Length: "+lengthofsnake, 780,50);
		
		//Initialize the default position of the snake at the top left corner:
		rightmouth=new ImageIcon("rightmouth.png");
		rightmouth.paintIcon(this, g, snakexlength[0], snakeylength[0]);
		
		for(int a=0;a<lengthofsnake;a++) {
			//detect the direction of snake:
			if(a==0 && right) {
				rightmouth=new ImageIcon("rightmouth.png");
				rightmouth.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			}
			
			if(a==0 && left) {
				leftmouth=new ImageIcon("leftmouth.png");
				leftmouth.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			}
			
			if(a==0 && down) {
				downmouth=new ImageIcon("downmouth.png");
				downmouth.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			}
			
			if(a==0 && up) {
				upmouth=new ImageIcon("upmouth.png");
				upmouth.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			}
			
			if(a!=0) {
				//a identifies the first index of snake,if a is not 0,it means snake's head is drawn and now we need to draw the body of the snake
			    //to draw the body of snake,we use snakeimage
				snakeimage=new ImageIcon("snakeimage.png");
				snakeimage.paintIcon(this, g, snakexlength[a], snakeylength[a]);
				
			}
		}
		
		
		//detect if the head of the snake collides with the pickup.
		//first print and draw the pick up on the panel and then check if the pickup has collided with the snakehead or not.
		
		//draw:
		enemyimage=new ImageIcon("enemy.png");
		
		//check collision:
		if((enemyxpos[xpos]==snakexlength[0] && enemyypos[ypos]==snakeylength[0])) {
			
			//increment score
			score++;
			
			//increment the length of snake if c/d is true:
			lengthofsnake++;
			
			//regenerate the pickup in the new position:
			xpos=random.nextInt(34);
			ypos=random.nextInt(23);
		}
		
	//print the icon on panel:
		enemyimage.paintIcon(this, g, enemyxpos[xpos], enemyypos[ypos]);
		
		//check the collision of snake's head with its body using a loop:
		
		for(int b=1;b<lengthofsnake;b++) {
			if(snakexlength[b]==snakexlength[0] && snakeylength[b]==snakeylength[0]) {
				//set all the variables that let us move in all directions to false:
				right=false;
				left=false;
				up=false;
				down=false;
				
				g.setColor(Color.white);
				g.setFont(new Font("monospaced",Font.BOLD,50));
				g.drawString("Game Over ", 300, 300);
				
				//press spacebar for restarting the game:
				g.setFont(new Font("monospaced",Font.BOLD,20));
				g.drawString("Space to RESTART", 350, 340);
			}
		}
		
		
		g.dispose();
	}

	@Override
	//method we need to override for action listener:
	public void actionPerformed(ActionEvent arg0) {
		timer.start();
		
		//check which direction's variable is true:
		if(right) {
			for(int r=lengthofsnake-1;r>=0;r--)  //why?
				{
				//shift the position of head to its next index:
				snakeylength[r+1]=snakeylength[r];
			}
			
			for(int r=lengthofsnake;r>=0;r--) {
				//shift the postion of snakexlength
				
				if(r==0) {
					snakexlength[r]=snakexlength[r]+25;
				}
				else {
					snakexlength[r]=snakexlength[r-1];
				}
				
				//if the snake is moving and touches the border on the right side,it should come out from the left side.For that,we check the position of the head.
				if(snakexlength[r]>850) {
					snakexlength[r]=25;
				}
			}
			repaint(); //calls paint method automatically.
		}
		
		if(left) {
			for(int r=lengthofsnake-1;r>=0;r--)  //why?
			{
			//shift the position of head to its next index:
			snakeylength[r+1]=snakeylength[r];
		}
		
		for(int r=lengthofsnake;r>=0;r--) {
			//shift the postion of snakexlength
			
			if(r==0) {
				snakexlength[r]=snakexlength[r]-25;
			}
			else {
				snakexlength[r]=snakexlength[r-1];
			}
			
			//if the snake is moving and touches the border on the left side,it should come out from the right side.For that,we check the position of the head.
			if(snakexlength[r]<25) {
				snakexlength[r]=850;
			}
		}
		repaint(); //calls paint method automatically.
		}
		
		if(up) {
			for(int r=lengthofsnake-1;r>=0;r--)  //why?
			{
			//shift the position of head to its next index:
			snakexlength[r+1]=snakexlength[r];
		}
		
		for(int r=lengthofsnake;r>=0;r--) {
			//shift the postion of snakexlength
			
			if(r==0) {
				snakeylength[r]=snakeylength[r]-25;
			}
			else {
				snakeylength[r]=snakeylength[r-1];
			}
			
			//if the snake is moving and touches the border on the up side,it should come out from the down side.For that,we check the position of the head.
			if(snakeylength[r]<75) {
				snakeylength[r]=625;
			}
		}
		repaint(); //calls paint method automatically.
		}
		
		if(down) {
			for(int r=lengthofsnake-1;r>=0;r--)  //why?
			{
			//shift the position of head to its next index:
			snakexlength[r+1]=snakexlength[r];
		}
		
		for(int r=lengthofsnake;r>=0;r--) {
			//shift the postion of snakexlength
			
			if(r==0) {
				snakeylength[r]=snakeylength[r]+25;
			}
			else {
				snakeylength[r]=snakeylength[r-1];
			}
			
			//if the snake is moving and touches the border on the down side,it should come out from the up side.For that,we check the position of the head.
			if(snakeylength[r]>625) {
				snakeylength[r]=75;
			}
		}
		repaint(); //calls paint method automatically.
		}
		
	}

	@Override
	//method(s)we need to override for key listener:
	//detect which arrow key is pressed and move the snake in that direction based upon it:
	//to detect the key we use e object of class keyevent
	//class keyevent has a method getkey code which lets us know which key is pressed
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_SPACE) {
			
			//re assign the variables to reset the game
			moves=0;
			score=0;
			lengthofsnake=3;
			right=true;
			left=false;
			up=false;
			down=false;
			repaint();
		}
		if(e.getKeyCode()==KeyEvent.VK_RIGHT) {
			
			//use the 4 variables declared above for direction
			//if the right key is pressed ,right will be assigned true and rest false.
			
			moves++; 
			right=true;
			if(!left) {
				right=true;
			}
			else {
				right=false;
				left=true;
			}
			
			up=false;
			down=false;
		}
		
if(e.getKeyCode()==KeyEvent.VK_LEFT) {
			
			//use the 4 variables declared above for direction
			//if the left key is pressed ,left will be assigned true and rest false.
			
			moves++; 
			left=true;
			if(!right) {
				left=true;
			}
			else {
				left=false;
				right=true;
			}
			
			up=false;
			down=false;
		}

if(e.getKeyCode()==KeyEvent.VK_UP) {
	
	//use the 4 variables declared above for direction
	//if the up key is pressed ,up will be assigned true and rest false.
	
	moves++; 
	up=true;
	if(!down) {
		up=true;
	}
	else {
		up=false;
		down=true;
	}
	
	left=false;
	right=false;
}

if(e.getKeyCode()==KeyEvent.VK_DOWN) {
	
	//use the 4 variables declared above for direction
	//if the down key is pressed ,down will be assigned true and rest false.
	
	moves++; 
	down=true;
	if(!up) {
		down=true;
	}
	else {
		down=false;
		up=true;
	}
	
	left=false;
	right=false;
}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
